#pragma once

//NORMALIZE DATA, standard deviation = 1, mean = 0
void z_score_normalization(std::vector<std::vector<double>>& data)
{
	for (int j = 0; j < data[0].size(); j++)
	{
		int n = data.size();
		double mean;
		double mean_of_squares;
		double sum = 0;
		double sum_of_squares = 0;
		for (int i = 0; i < n; i++)
		{
			sum += (data[i])[j];
			sum_of_squares += (data[i])[j] * (data[i])[j];
		}
		mean = sum / n;
		mean_of_squares = sum_of_squares / n;

		double standard_deviation = sqrt((n / (n - 1)) * (mean_of_squares - mean * mean));

		for (int i = 0; i < n; i++)
		{
			(data[i])[j] = ((data[i])[j] - mean) / standard_deviation;
		}
	}
}