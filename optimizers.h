#pragma once

void wait(double s) {
	double current_time = omp_get_wtime();
	while (current_time + s > omp_get_wtime());
}

struct hyper_parameters {
	int epoks;
	double learning_rate;
};

//Serial, classic gradient descent
void gradient_descent(model& function, const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const hyper_parameters hp)
{
	std::vector<double> p;
	function.get_parameters(p);
	std::vector<double> g(p.size());

	for (int i = 0; i < hp.epoks; i++)
	{
		function.calc_gradient(feature_data, label_data, p, g);
		//Apply gradients
		for (int j = 0; j < p.size(); j++)
			p[j] += g[j] * hp.learning_rate;

#ifdef _DEBUG
		std::cout << "PARAMETERS: " << '\t';
		for (int i = 0; i < p.size(); i++)
			std::cout << p[i] << '\t';
		std::cout << '\n';
#endif //_DEBUG
	}
	function.set_parameters(p);
	print_parameters(p);
}

//Serial SGD, samples 1 data and calculate gradient.
void sgd(model& function, const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const hyper_parameters hp)
{
	std::vector<double> p;
	function.get_parameters(p);
	std::vector<double> g(p.size());

	std::vector<std::vector<double>> feature_data_sample(1);
	std::vector<int> label_data_sample(1);

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<unsigned int> sample_index(0, label_data.size() - 1);

	for (int i = 0; i < hp.epoks * label_data.size(); i++)
	{
		//Create subset of feature and label by sampling.
		unsigned int rand_numb = sample_index(gen);
		//int rand_numb = rand() % label_data.size();
		feature_data_sample[0] = feature_data[rand_numb];
		label_data_sample[0] = label_data[rand_numb];

		//std::cout << "Data used: " << feature_data_sample[0][0] << ", " << feature_data_sample[0][1] << ", " << feature_data_sample[0][2] << ", " << feature_data_sample[0][3] << ", " << label_data_sample[0] << "\n";

		//Would it be better to give gradient calculater all data and the info of what data to pick?

		function.calc_gradient(feature_data_sample, label_data_sample, p, g);
		//Apply gradients
		for (int j = 0; j < p.size(); j++)
			p[j] += g[j] * hp.learning_rate / label_data.size();

#ifdef _DEBUG
		std::cout << "PARAMETERS: " << '\t';
		for (int i = 0; i < p.size(); i++)
			std::cout << p[i] << '\t';
		std::cout << '\n';
#endif //_DEBUG
	}
	function.set_parameters(p);
	print_parameters(p);
}

//Sync Parallel SGD, samples 1 data and calculate gradient. Agregate each workers result.
//First the threads calculate the gradient. Then they synchronize and apply the gradient.
//Alternativly one could implement a solution where each thread has a copy of the parameter vector instead. 1 less synchronization sten then.
void psgd(model& m, const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const hyper_parameters hp)
{
	std::vector<double> p;
	m.get_parameters(p);

	double start_time = omp_get_wtime();
	volatile int i = 0;
	int iterations = hp.epoks * label_data.size() * 1.0 / NUM_WORKERS;
#pragma omp parallel num_threads(NUM_WORKERS)
	{
		std::vector<std::vector<double>> feature_data_sample(1);
		std::vector<int> label_data_sample(1);
		std::vector<double> g(p.size());

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<unsigned int> sample_index(0, label_data.size() - 1);

		//Unsolvable with for loop that went over chunks of N workers, then waited.
		while (i < iterations)
		{
#pragma omp for schedule(static, 1)
			for (int j = 0; j < NUM_WORKERS; j++)
			{
				//Create subset of feature and label by sampling.
				unsigned int rand_numb = sample_index(gen);
				feature_data_sample[0] = feature_data[rand_numb];
				label_data_sample[0] = label_data[rand_numb];
				m.calc_gradient(feature_data_sample, label_data_sample, p, g);
			}
			//Apply gradients
#pragma omp critical
			for (int j = 0; j < p.size(); j++)
				p[j] += g[j] * hp.learning_rate / label_data.size();
#pragma omp single
			i++;
		}
	}
	double time = omp_get_wtime() - start_time;
	std::cout << "INFO: Run time: " << time << "\n";

	m.set_parameters(p);
	print_parameters(p);
}

void lock_free_async_psgd(model& m, const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const hyper_parameters hp)
{
	std::vector<double> p;
	m.get_parameters(p);

	double start_time = 0;
#pragma omp parallel num_threads(NUM_WORKERS)
	{
		std::vector<std::vector<double>> feature_data_sample(1);
		std::vector<int> label_data_sample(1);

		//Give each worker a gradient vector and parameter vector
		std::vector<double> wp(p.size());
		std::vector<double> g(p.size());

		//Give each worker a unique random generator called sample index.
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<unsigned int> sample_index(0, label_data.size() - 1);

#pragma omp single
		start_time = omp_get_wtime();
#pragma omp for
		for (int i = 0; i < hp.epoks * label_data.size(); i++)
		{
			//Create subset of feature and label by sampling.
			unsigned int rand_numb = sample_index(gen);
			feature_data_sample[0] = feature_data[rand_numb];
			label_data_sample[0] = label_data[rand_numb];
			//#pragma omp critical //Add critical section for locking
			wp = p;
			m.calc_gradient(feature_data_sample, label_data_sample, wp, g);
			//#pragma omp critical //Add critical section for locking
			for (int j = 0; j < p.size(); j++)
				p[j] += g[j] * hp.learning_rate / label_data.size();
		}
	}
	double time = omp_get_wtime() - start_time;
	std::cout << "INFO: Run time: " << time << std::endl;

	m.set_parameters(p);
	print_parameters(p);
}

/*
void distributed_async_psgd(model& m, const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const hyper_parameters hp)
{
	std::vector<double> p;
	m.get_parameters(p);

	double start_time, time;

	int running = 1;

#pragma omp parallel num_threads(NUM_WORKERS)
	{

#pragma omp master
		while (running) {}

		std::vector<std::vector<double>> feature_data_sample(1);
		std::vector<int> label_data_sample(1);

		//Give each worker a gradient vector
		std::vector<double> g(p.size());

		//Give each worker a unique random generator called sample index.
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<unsigned int> sample_index(0, label_data.size() - 1);

		std::cout << "My id: " << omp_get_thread_num() << std::endl;

#pragma omp single nowait
		start_time = omp_get_wtime();

#pragma omp for
		for (int i = 0; i < hp.epoks * label_data.size(); i++)
		{
			//Create subset of feature and label by sampling.
			unsigned int rand_numb = sample_index(gen);
			feature_data_sample[0] = feature_data[rand_numb];
			label_data_sample[0] = label_data[rand_numb];
			m.calc_gradient(feature_data_sample, label_data_sample, p, g);
			//#pragma omp critical //Add critical section for locking
			for (int j = 0; j < p.size(); j++)
				p[j] += g[j] * hp.learning_rate / label_data.size();
		}

#pragma omp single nowait
		time = omp_get_wtime() - start_time;
		running = 0;
		std::cout << "DONE!" << std::endl;
	}
	std::cout << "INFO: Run time: " << time << std::endl;

	m.set_parameters(p);
	print_parameters(p);
}
*/