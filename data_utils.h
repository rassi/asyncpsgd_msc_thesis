#pragma once

//IRIS DATA SET
void get_data_iris(std::vector<std::vector<double>>& feature_data, std::vector<int>& label_data, std::map<int, std::string>& label_to_name)
{
	//feature_data.resize(4);
	std::ifstream infile("C:/Users/rasmus/Workspace/C++/logistic_regression_with_psgd/logistic_regression_with_psgd/iris.data");
	
	double a, b, c, d;
	std::string name;
	char tmp;
	int index = 0;
	int row = 0;

	std::map<std::string, int> mapofwords;

	while (infile >> a >> tmp >> b >> tmp >> c >> tmp >> d >> tmp >> name)
	{
		feature_data.push_back(std::vector<double>());
		(feature_data[row]).push_back(a);
		(feature_data[row]).push_back(b);
		(feature_data[row]).push_back(c);
		(feature_data[row]).push_back(d);
		row++;

		if (mapofwords.insert(std::make_pair(name, index)).second == false)
			label_data.push_back(mapofwords[name]);
		else
		{
			label_data.push_back(index);
			label_to_name.insert(std::make_pair(index, name));
			index++;
		}
	}
#ifdef _DEBUG
	std::cout << "Rows: " << feature_data.size() << "\nCols: " << feature_data[0].size() << "\n";
#endif //_DEBUG
}

void print_data(const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data)
{
	for (auto row = 0; row < feature_data.size(); row++)
	{
		std::cout << "ROW " << row << ":\t";
		for (int i = 0; i < 4; i++)
			std::cout << (feature_data[row])[i] << '\t';
		std::cout << label_data[row] << '\n';
	}
}

void print_parameters(const std::vector<double>& p)
{
	std::cout << "PARAMETERS: " << '\t';
	for (int i = 0; i < p.size(); i++)
		std::cout << p[i] << '\t';
	std::cout << '\n';
}
