//AUTHOR:	Rasmus Edvardsson
//EMAIL:	rasedv@student.chalmers.se

//INSTRUCTIONS
//Only tested on Windows 10
//Run with OpenMP 2.0 enabled, with /Zc:twoPhase- compiler command for Windows.
//Choose optimization methon in this file, and also epoks and learning rate.
//In data_utils.h set the location of the iris data file.
//Run in debug mode for letting your command prompt be filled with parameter and gradient information goodness.

//GENERAL LIBRARIES
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <omp.h>
#include <iomanip>
#include <random>

//CONSTANTS
const unsigned int NUM_CPU = 6;
const unsigned int NUM_WORKERS = NUM_CPU;

#include "data_utils.h"
#include "statistics.h"
#include "models.h"
#include "optimizers.h"

int main()
{
	//Set number of workers
	omp_set_num_threads(NUM_CPU);

	//Print double with only 3 digits precision, makes the terminal less floded.
	//std::cout << std::setprecision(3);

	//GET DATA
	std::vector<std::vector<double>> feature_data;
	std::vector<int> label_data;
	std::map<int, std::string> label_to_name; //Used for going from arbitrary naming to human naming.
	get_data_iris(feature_data, label_data, label_to_name);

	//Remove data with label 1 as to logistic regression only is binary
	for (int row = 100; row < label_data.size(); row++)
		label_data[row] = 1;
	label_data.erase(label_data.end() - 100, label_data.end() - 50);
	feature_data.erase(feature_data.end() - 100, feature_data.end() - 50);
	label_to_name.erase(1);

	//NORMALIZE DATA, standard deviation = 1, mean = 0
	z_score_normalization(feature_data);

	//Print data
#ifdef _DEBUG
	print_data(feature_data, label_data);
#endif //_DEBUG

	//CREATE MODEL
	logistic_regression lr = logistic_regression(feature_data[0].size());

	//SET OPTIMIZERS HYPER PARAMETERS
	hyper_parameters hp;
	hp.epoks = 100;
	hp.learning_rate = 0.1;
	
	//USE ONE OF THE FOLLOWING OPTIMIZATION ALGORITHMS
	gradient_descent(lr, feature_data, label_data, hp);
	//sgd(lr, feature_data, label_data, hp);
	//psgd(lr, feature_data, label_data, hp);
	//lock_free_async_psgd(lr, feature_data, label_data, hp);

	//VERIFY RESULTS
	int correct = 0;
	for (int i = 0; i < label_data.size(); i++)
	{
		double prob = lr.predict(feature_data[i]);

		std::cout << "Data: " << i << " Probability: " << prob << '\n';
		if (prob > 0.5 && label_data[i] == 1)
			correct++;
		else if (prob <= 0.5 && label_data[i] == 0)
			correct++;
	}
	std::cout << "correct: " << correct << '\n';
}