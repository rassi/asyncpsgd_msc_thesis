#pragma once

class model
{
public:
	std::string get_name() {
		return name;
	}
	void set_parameters(const std::vector<double>& p) {
		mp.clear();
		mp.insert(mp.end(), p.begin(), p.end());
	}
	void get_parameters(std::vector<double>& p) {
		p.clear();
		p.insert(p.end(), mp.begin(), mp.end());
	}
	virtual void calc_gradient(const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const std::vector<double>& p, std::vector<double>& g) = 0;
	virtual double predict(const std::vector<double>& feature_data) = 0;

protected:
	std::string name;
	std::vector<double> mp;
	model() {} //Gets called before derived classes.
};

class logistic_regression : public model {
public:
	logistic_regression(const int features) {
		name = "Logistic Regression";
		mp.assign(features + 1, 1);
	}
	void calc_gradient(const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const std::vector<double>& p, std::vector<double>& g) {
		double denominator = 0;
		std::vector<double> gtmp(g.size(), 0);
		for (int row = 0; row < label_data.size(); row++)
		{
			denominator = 1 / (1 + exp((p[0]
				+ p[1] * (feature_data[row])[0]
				+ p[2] * (feature_data[row])[1]
				+ p[3] * (feature_data[row])[2]
				+ p[4] * (feature_data[row])[3])))
				- 1 + label_data[row];

			gtmp[0] += denominator;
			for (int i = 0; i < gtmp.size() - 1; i++)
				gtmp[i + 1] += denominator * (feature_data[row])[i];
		}
		for (int i = 0; i < g.size(); i++)
			g[i] = gtmp[i] / label_data.size();

#ifdef _DEBUG
		std::cout << "GRADIENTS:  " << '\t';
		for (int i = 0; i < g.size(); i++)
			std::cout << g[i] << '\t';
		std::cout << '\n';
#endif //_DEBUG
	}
	double predict(const std::vector<double>& feature_data) {
		double prob = 1 / (1 + exp(-(mp[0] +
			mp[1] * (feature_data[0]) +
			mp[2] * (feature_data[1]) +
			mp[3] * (feature_data[2]) +
			mp[4] * (feature_data[3]))));

		return prob;
	}
};

/*
class multinomial_logistic_regression : public model {
public:
	multinomial_logistic_regression(const int features, const int classes) {
		name = "Multinomial Logistic Regression";
		mp.assign((features + 1) * classes, 1);

		sub_models.resize
	}
	void calc_gradient(const std::vector<std::vector<double>>& feature_data, const std::vector<int>& label_data, const std::vector<double>& p, std::vector<double>& g) {

	}
	double predict(const std::vector<double>& feature_data) {

		return 0;
	}
private:
	std::vector<logistic_regression> sub_models;
};
*/